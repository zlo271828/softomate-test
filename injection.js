chrome.storage.local.get(["siteList", "closed", "searchDomains"], function(storedValues) {
    if (storedValues.siteList != null) {
        const list = storedValues.siteList;
        const domains = list.reduce(function (domains, domain, index) {
            domains[index] = domain.domain;
            return domains;
        }, []);
        const searchDomains = storedValues.searchDomains;

        const domain = location.hostname.replace('www.', '');
        if (searchDomains.includes(domain)) {
            let items;
            switch (domain.split('.')[0]) {
                case 'google':
                    items = document.getElementsByClassName("rc");
                    for (let j = 0; j < items.length; j++) {
                        const domain = extractHostname(items[j].children[1].children[0].children[0].children[0].innerHTML);
                        if (domains.includes(domain)) {
                            const name = items[j].children[0].children[0];
                            const icon = document.createElement('img');
                            icon.src = chrome.extension.getURL("icon.png");
                            icon.className = 'injection-ext__icon';
                            items[j].children[0].insertBefore(icon, name);
                        }
                    }
                    break;
                case 'bing':
                    items = document.getElementsByClassName("b_algo");
                    console.log(items);
                    for (let j = 0; j < items.length; j++) {
                        const domain = extractHostname(items[j].getElementsByTagName('cite')[0].innerHTML.replace('<strong>', '').replace('</strong>', ''));
                        console.log(domain);
                        if (domains.includes(domain)) {
                            const name = items[j].children[0].firstChild;
                            const icon = document.createElement('img');
                            icon.src = chrome.extension.getURL("icon.png");
                            icon.className = 'injection-ext__icon';
                            items[j].children[0].insertBefore(icon, name);
                        }
                    }
                    break;
            }
        }
        for (let i = 0; i < list.length; i++) {
            if (list[i].domain === domain && !isClosed(list[i], storedValues.closed)) {
                list[i].count = list[i].count + 1 || 1;
                if (list[i].count > 3) {
                    storedValues.closed.push(list[i]);
                    chrome.storage.local.set({ "closed": storedValues.closed });
                    continue;
                }
                chrome.storage.local.set({ "siteList": list });
                const injection = document.createElement("div");
                const msg = document.createTextNode(list[i].message);
                const close = document.createElement('div');
                injection.addEventListener('click', function () {
                    storedValues.closed.push(list[i]);
                    chrome.storage.local.set({ "closed": storedValues.closed });
                    injection.remove();
                    return false;
                });
                close.id = 'injection-ext__button';
                close.innerText = 'Click to close this message...';
                injection.appendChild(msg);
                injection.appendChild(close);
                injection.id = 'injection-ext';
                document.getElementsByTagName("body")[0].insertBefore(injection, document.getElementsByTagName("body")[0].children[0]);
            }
        }
    }
});

function isClosed(item, closedList) {
    for (let i = 0; i < closedList.length; i++)
        if (item.domain === closedList[i].domain) return true;
    return false;
}

function extractHostname(url) {
    let hostname;
    if (url.indexOf("://") > -1) hostname = url.split('/')[2];
    else hostname = url.split('/')[0];
    hostname = hostname.split(':')[0];
    hostname = hostname.split('?')[0];
    return hostname.replace('www.', '');
}