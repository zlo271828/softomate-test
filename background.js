function fetchListHandler() {
    fetch('http://www.softomate.net/ext/employees/list.json')
        .then(function (response) {
            return response.json();
        })
        .then(function (list) {
            chrome.storage.local.set({ "siteList": list});
        });
}

chrome.runtime.onStartup.addListener(function() {
    fetchListHandler();
    chrome.storage.local.set({ "closed": [] });
    chrome.storage.local.set({ "searchDomains": ['google.com', 'google.ru', 'bing.com'] });
    setInterval(fetchListHandler, 3600000);
});


